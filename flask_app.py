# A very simple Flask Hello World app for you to get started with...

from flask import Flask, redirect, url_for, render_template, request
from flask_login import LoginManager, UserMixin,  current_user, login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash
from pony.orm import Database, Required, Optional, db_session, select
import keygen
from forms import LoginForm



app = Flask(__name__)
app.secret_key = keygen.generate()
login = LoginManager(app)
login.login_view = 'login'

db = Database()

class User(UserMixin, db.Entity):

    username = Required(str, unique=True)
    password_hash = Optional(str)

    @db_session
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    @db_session
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)

db.bind(provider='sqlite', filename='mydb', create_db=True)
db.generate_mapping(create_tables=True)


@login.user_loader
@db_session
def load_user(id):
    return User.get(id=int(id))


@app.route('/')
@login_required
def index():
    return render_template('index.html', NAME=current_user.username)

@app.route('/login', methods=['GET', 'POST'])
@db_session
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.get(username=form.username.data)

        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))

        login_user(user)  # remember=form.remember_me.data)
        return redirect(url_for('index'))

    return render_template('login.html', title='Sign In', form=form)

@app.route('/new_user', methods=['GET', 'POST'])
@db_session
def new_user_form():
    if request.method == 'GET':
        return render_template('newuserform.html')

    elif request.method == 'POST':
        data = request.form.to_dict()

        u = User(username=data['username'])
        u.set_password(data['password'])

        return redirect(url_for('index'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')

